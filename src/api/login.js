import request from '@/utils/request'
import {getToken} from "../utils/auth";

export default {
  login(UmsMember) {
    return request({
      url: '/login',
      method: 'post',
      data: {
        username: UmsMember.username, password: UmsMember.password
      }
    })
  }
}

export function getInfo() {
  return request({
    url: '/getUserName',
    method: 'post',
    headers :{
      Authorization:getToken()
    },
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
