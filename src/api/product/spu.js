import request from '@/utils/request'
import {getToken} from "../../utils/auth";

export default {

  // 根据三级分类id获取属性列表
  getSpuList(catalog3Id,pageNum,pageSize) {
    return request({
      headers :{
        Authorization:getToken()
      },
      url: 'spuList?catalog3Id=' + catalog3Id+"&pageNum="+pageNum+"&pageSize="+pageSize,
      method: 'get'
    })
  },
  //分页获取列表
  getList(catalog3Id,pageNum,pageSize){
    return request({
      url: '/goodslist?catalog3Id='+catalog3Id+"&pageNum="+pageNum+"&pageSize="+pageSize,
      method: 'get'
    })
  },

  // 保存Spu
  saveSpuInfo(spuForm) {
    return request({
      headers :{
        Authorization:getToken()
      },
      url: 'saveSpuInfo',
      method: 'post',
      data: spuForm
    })
  },

  // 获取基本销售属性列表
  getBaseSaleAttrList() {
    return request({
      headers :{
        Authorization:getToken()
      },
      url: 'baseSaleAttrList',
      method: 'post'
    })
  },

  // 根据spuId获取销售属性列表
  getSpuSaleAttrList(spuId) {
    return request({
      headers :{
        Authorization:getToken()
      },
      url: 'spuSaleAttrList?spuId=' + spuId,
      method: 'get'
    })
  },

  // 根据spuId获取图片列表
  getSpuImageList(spuId) {
    return request({
      headers :{
        Authorization:getToken()
      },
      url: 'spuImageList?spuId=' + spuId,
      method: 'get'
    })
  }
}
