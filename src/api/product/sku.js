import request from '@/utils/request'
import {getToken} from "../../utils/auth";

export default {

  // 保存Sku
  saveSkuInfo(skuForm) {
    return request({
      headers :{
        Authorization:getToken()
      },
      url: 'saveSkuInfo',
      method: 'post',
      data: skuForm
    })
  }
}
